import { pipe } from "fp-ts/lib/function"
import * as O from "fp-ts/lib/Option"

interface Data {
  aNumber?: number
}

const timesTwo = (data: Data) =>
  pipe(
    O.fromNullable(data.aNumber),
    O.map((n) => n * 2)
  )

console.log(timesTwo({}))
console.log(
  timesTwo({
    aNumber: 5,
  })
)
