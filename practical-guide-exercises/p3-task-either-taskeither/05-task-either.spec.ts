import { assertLeft, assertRight } from '../utils'
import { getBalanceTaskEither } from './05-task-either'

describe('task-either', () => {
  it('succeeds if account.123 is supplied', async () => {
    const result = await getBalanceTaskEither('account.123')()
    assertRight(result)
    expect(result.right).toStrictEqual(1023.52)
  })
  it('fails with GetBalanceError if account.234 is supplied', async () => {
    const result = await getBalanceTaskEither('account.456')()
    assertLeft(result)
    expect(result.left.type).toStrictEqual('GET_BALANCE_ERROR')
  })
})
