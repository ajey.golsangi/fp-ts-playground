import { getBalanceTask } from './06-task-either-fold'

describe('task-either', () => {
  it('succeeds if account.123 is supplied', async () => {
    const result = await getBalanceTask('account.123')()
    expect(result).toStrictEqual(1023.52)
  })
  it('returns -1 if account.234 is supplied', async () => {
    const result = await getBalanceTask('account.456')()
    expect(result).toStrictEqual(-1)
  })
})
