/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - Modify add5Task to fix the type error by convering number to a task
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 00-task.ts
 *
 * Testing this file.
 *  - npm run test 00-task
 */

import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../utils'

const add5 = (num: number) => num + 5

export const add5Task = (number: number): T.Task<number> =>
  pipe(
    // TODO modify this line to convert the number variable to a Task<number>
    // and fix the type error
    number,
    T.map(add5)
  )

// No need to modify below here, for running this file
const logAndReturnResult = (number: number) =>
  pipe(
    add5Task(number),
    T.map(result =>
      pipe(
        console.dir(`add5Task(${number}):`),
        () => console.dir(result),
        () => result
      )
    )
  )

pipe(logAndReturnResult(5), T.chain(logAndReturnResult), runIfCli(module))
