/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - getBalanceFromMockApi is a promise that can fail.  Implement getBalanceTaskEither
 *    using TE.tryCatch to make the call to getBalanceFromMockApi safe from promise rejections
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 05-task-either.ts
 *
 * Testing this file.
 *  - npm run test 05-task-either
 */

import { flow, pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/lib/TaskEither'
import { runIfCli } from '../../utils'

class GetBalanceError extends Error {
  public readonly type = 'GET_BALANCE_ERROR'
  constructor(public readonly error: unknown) {
    super()
  }
}

type AccountId = 'account.123' | 'account.456'

const getBalanceFromMockApi = async (accountId: AccountId) => {
  if (accountId === 'account.456') {
    throw new Error('bad account')
  }
  return 1023.52
}

export const getBalanceTaskEither = (
  accountId: AccountId
): TE.TaskEither<GetBalanceError, number> =>
  TE.tryCatch(
    () => getBalanceFromMockApi(accountId),
    e => new GetBalanceError(e)
  )

// TODO this also works, but it is less explicit and may be more difficult
// for someone reading this function for the first time to determine what is happening.
export const getBalanceTaskEitherAlternate = flow(
  TE.tryCatchK(getBalanceFromMockApi, e => new GetBalanceError(e))
)

// No need to modify below here, for running this file
const logResult = (accountId: AccountId) =>
  pipe(
    TE.fromIO(() => console.dir(`getBalanceFromMockApi(${accountId}):`)),
    TE.chainW(() => getBalanceTaskEither(accountId)),
    TE.bimap(console.dir, console.dir)
  )

pipe(['account.123', 'account.456'], TE.traverseSeqArray(logResult), runIfCli(module))
