/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - There seems to be a type error in multiplyPositiveFractionNumberBy5.
 *    Figure out what the problem is and fix it.
 *  - Do not modify positiveNumber, fractionNumber, or any code after the => in
 *    multiplyPositiveFractionNumberBy5
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 04-either-left-types.ts
 *
 * Testing this file.
 *  - No tests for this file
 */

import * as E from 'fp-ts/lib/Either'
import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../../utils'

class PositiveNumberError extends Error {
  public readonly type = 'POSITIVE_NUMBER_ERROR'
}

class FractionNumberError extends Error {
  public readonly type = 'FRACTION_NUMBER_ERROR'
}

const positiveNumber = (number: number): E.Either<PositiveNumberError, number> =>
  number < 0 ? E.left(new PositiveNumberError(`${number} is not positive.`)) : E.right(number)

const fractionNumber = (number: number): E.Either<FractionNumberError, number> =>
  number <= -1 || number >= 1
    ? E.left(new FractionNumberError(`${number} is not a fraction.`))
    : E.right(number)

/*
 * TODO
 *  - There seems to be a type error in multiplyPositiveFractionNumberBy5.
 *    Figure out what the problem is and fix it.
 *  - Do not modify positiveNumber, fractionNumber, or any code after the => in
 *    multiplyPositiveFractionNumberBy5
 */
export const multiplyPositiveFractionNumberBy5 = (
  number: number
): E.Either<PositiveNumberError | FractionNumberError, number> =>
  pipe(
    positiveNumber(number),
    E.chainW(fractionNumber),
    E.map(num => num * 5)
  )

// No need to modify below here, for running this file
const logResult = (number: number) =>
  pipe(console.dir(`multiplyPositiveFractionNumberBy5(${number}):`), () =>
    console.dir(multiplyPositiveFractionNumberBy5(number))
  )

pipe(
  T.fromIO(() =>
    pipe(
      logResult(10),
      () => logResult(-0.12),
      () => logResult(0.12)
    )
  ),
  runIfCli(module)
)
