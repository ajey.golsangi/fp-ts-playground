/**
 * Exercise
 * - You are tasked with creating an endpoint that retrieves all cards from a third-party for a given user
 *   based off of different parameters. Each file presented must be completed using fp-ts and io-ts.
 * - After completing every other file, update below where indicated to fix the type errors
 */

import { pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/TaskEither'
import { DisplayCard } from './01-types'
import { getAndParseCards } from './02-get-and-parse-cards'
import { validateCards } from './03-validate-cards'
import { formatCards } from './04-format-cards'
import { CardNetwork, CardStatus, GetCards } from './card-api'
import { CardErrors } from './errors'

interface I {
  // Retrieve all cards for a given userId
  userId: string
  // If networkType is provided, only return cards with the given networkType
  networkType?: CardNetwork
  // If cardStatus is provided, only return cards with the given cardStatus
  cardStatus?: CardStatus
  getCards: GetCards
}

export type GetCardsHandler = ({
  userId,
  networkType,
  cardStatus,
  getCards,
}: I) => TE.TaskEither<CardErrors, DisplayCard[]>

export const getCardsHandler: GetCardsHandler = ({ userId, networkType, cardStatus, getCards }) =>
  pipe(
    // 01-types.ts
    // 02-get-and-parse-cards.ts
    getAndParseCards(getCards)(userId),
    // 03-validate-cards.ts
    // TODO Update here
    parsedCards => validateCards({ networkType, cardStatus })(parsedCards),
    // 04-format-cards.ts
    // TODO Update here
    validatedCards => formatCards(validatedCards)
  )
