/**
 * Exercise
 * - Transform the input array of Cards to an array of DisplayCards with the following criteria
 * - id of the DisplayCard must match the id of the Card
 * - status of the DisplayCard must match the status of the Card
 * - network of the DisplayCard must match the network of the Card if present, otherwise default to DefaultNetwork
 * - display_name of the DisplayCard should match the following format: `Card x<last4>`
 *   - If last4 is not present on the Card, default to DefaultLast4
 * - mail_to_address of the DisplayCard must match the fulfillment.recipient_mailing_address if present, otherwise it must
 *   match fulfillment.recipient_address if present, otherwise default to undefined
 */

import { Card, DisplayCard } from './01-types'
import { pipe } from 'fp-ts/lib/function'
import * as A from 'fp-ts/Array'
import * as O from 'fp-ts/Option'

export const DefaultNetwork = 'Unknown'
export const DefaultLast4 = 'XXXX'

export const formatCards = (cards: Card[]): DisplayCard[] =>
  pipe(
    cards
    // TODO Implementation here
  )
