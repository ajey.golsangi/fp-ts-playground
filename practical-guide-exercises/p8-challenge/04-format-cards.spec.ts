import { Card } from './01-types'
import { formatCards, DefaultNetwork, DefaultLast4 } from './04-format-cards'

describe('format-cards', () => {
  it('should handle an empty array of Cards and return an empty DisplayCard array', () => {
    const cards: Card[] = []

    expect(formatCards(cards)).toStrictEqual([])
  })

  it('should handle cards with only required fields', () => {
    const cards: Card[] = [
      {
        id: '1',
        status: 'ACTIVE',
      },
      {
        id: '2',
        status: 'INACTIVE',
      },
    ]

    expect(formatCards(cards)).toEqual([
      {
        id: '1',
        status: 'ACTIVE',
        network: DefaultNetwork,
        display_name: `Card x${DefaultLast4}`,
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: DefaultNetwork,
        display_name: `Card x${DefaultLast4}`,
      },
    ])
  })

  it('should handle cards with network provided', () => {
    const cards: Card[] = [
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
      },
    ]

    expect(formatCards(cards)).toEqual([
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        display_name: `Card x${DefaultLast4}`,
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        display_name: `Card x${DefaultLast4}`,
      },
    ])
  })

  it('should handle cards with network and last4 provided', () => {
    const cards: Card[] = [
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        last4: '1234',
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        last4: '2345',
      },
    ]

    expect(formatCards(cards)).toEqual([
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        display_name: `Card x1234`,
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        display_name: `Card x2345`,
      },
    ])
  })

  it('should handle cards with recipient_mailing_address', () => {
    const cards: Card[] = [
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        last4: '1234',
        fulfillment: {
          recipient_mailing_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        last4: '2345',
        fulfillment: {
          recipient_mailing_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        },
      },
    ]

    expect(formatCards(cards)).toEqual([
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        display_name: `Card x1234`,
        mail_to_address: {
          first_name: 'Tyler',
          last_name: 'Okonma',
          address_1: '53 Beach St',
          address_2: '2nd Floor',
          city: 'New York',
          state: 'NY',
          zip: '10013',
          country: 'USA',
        },
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        display_name: `Card x2345`,
        mail_to_address: {
          first_name: 'Malcolm',
          last_name: 'McCormick',
          address_1: '2407 J St',
          address_2: 'Suite 300',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
      },
    ])
  })

  it('should handle cards with recipient_address', () => {
    const cards: Card[] = [
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        last4: '1234',
        fulfillment: {
          recipient_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        last4: '2345',
        fulfillment: {
          recipient_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        },
      },
    ]

    expect(formatCards(cards)).toEqual([
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        display_name: `Card x1234`,
        mail_to_address: {
          first_name: 'Tyler',
          last_name: 'Okonma',
          address_1: '53 Beach St',
          address_2: '2nd Floor',
          city: 'New York',
          state: 'NY',
          zip: '10013',
          country: 'USA',
        },
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        display_name: `Card x2345`,
        mail_to_address: {
          first_name: 'Malcolm',
          last_name: 'McCormick',
          address_1: '2407 J St',
          address_2: 'Suite 300',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
      },
    ])
  })

  it('should handle cards with recipient_address and recipient_mailing_address', () => {
    const cards: Card[] = [
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        last4: '1234',
        fulfillment: {
          recipient_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
          recipient_mailing_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        },
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        last4: '2345',
        fulfillment: {
          recipient_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
          recipient_mailing_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      },
    ]

    expect(formatCards(cards)).toEqual([
      {
        id: '1',
        status: 'ACTIVE',
        network: 'VISA',
        display_name: `Card x1234`,
        mail_to_address: {
          first_name: 'Malcolm',
          last_name: 'McCormick',
          address_1: '2407 J St',
          address_2: 'Suite 300',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        display_name: `Card x2345`,
        mail_to_address: {
          first_name: 'Tyler',
          last_name: 'Okonma',
          address_1: '53 Beach St',
          address_2: '2nd Floor',
          city: 'New York',
          state: 'NY',
          zip: '10013',
          country: 'USA',
        },
      },
    ])
  })
})
