import * as t from 'io-ts'
import { CardNetwork, CardStatus } from '../card-api'

type AddressBase = {
  first_name: string
  last_name: string
  address_1: string
  address_2?: string
  city: string
  state: string
  zip: string
  country?: string
}

type CardBase = {
  id: string
  status: CardStatus
  network?: CardNetwork
  last4?: string
  fulfillment?: {
    recipient_address?: AddressBase
    recipient_mailing_address?: AddressBase
  }
}

type DisplayCardBase = {
  id: string
  status: CardStatus
  network: string
  display_name: string
  mail_to_address?: AddressBase
}

const Address = t.intersection([
  t.type({
    first_name: t.string,
    last_name: t.string,
    address_1: t.string,
    city: t.string,
    state: t.string,
    zip: t.string,
  }),
  t.partial({
    address_2: t.string,
    country: t.string,
  }),
])

const Status = t.union([t.literal('ACTIVE'), t.literal('INACTIVE')])

const Network = t.union([t.literal('VISA'), t.literal('MASTERCARD')])

export const Card = t.intersection([
  t.type({
    id: t.string,
    status: Status,
  }),
  t.partial({
    network: Network,
    last4: t.string,
    fulfillment: t.partial({
      recipient_address: Address,
      recipient_mailing_address: Address,
    }),
  }),
])

export const DisplayCard = t.intersection([
  t.type({
    id: t.string,
    status: Status,
    network: t.string,
    display_name: t.string,
  }),
  t.partial({
    mail_to_address: Address,
  }),
])

export type Card = t.TypeOf<typeof Card>
export type DisplayCard = t.TypeOf<typeof DisplayCard>

export const CardArray = t.array(Card)
export type CardArray = t.TypeOf<typeof CardArray>
