import { Card, DisplayCard } from './01-types'
import { pipe } from 'fp-ts/lib/function'
import * as A from 'fp-ts/Array'
import * as O from 'fp-ts/Option'

export const DefaultNetwork = 'Unknown'
export const DefaultLast4 = 'XXXX'

export const formatCards = (cards: Card[]): DisplayCard[] =>
  pipe(
    cards,
    A.map(card => ({
      id: card.id,
      status: card.status,
      network: pipe(
        O.fromNullable(card.network),
        O.getOrElseW(() => DefaultNetwork)
      ),
      display_name: pipe(
        O.fromNullable(card.last4),
        O.getOrElseW(() => DefaultLast4),
        last4 => `Card x${last4}`
      ),
      mail_to_address: pipe(
        O.fromNullable(card.fulfillment),
        O.chainNullableK(fulfillment => fulfillment.recipient_mailing_address),
        O.getOrElseW(() =>
          pipe(
            O.fromNullable(card.fulfillment),
            O.chainNullableK(fulfillment => fulfillment.recipient_address),
            O.toUndefined
          )
        )
      ),
    }))
  )
