/**
 * Exercise
 * - Update getAndParseCards to retrieve cards using getCards, then validate that
 *   the returned cards match the Card type from 01-types.ts
 * - If getCards returns an error, return TE.left(GetCardNetworkError)
 * - If no cards are returned from getCards, return TE.left(CardsNotFoundError)
 * - If any of the cards returned from getCards do not match the Card type, return TE.left(IoTsValidationError)
 */

import { GetCards } from './card-api'
import * as TE from 'fp-ts/TaskEither'
import * as NEA from 'fp-ts/NonEmptyArray'
import * as t from 'io-ts'
import * as E from 'fp-ts/Either'
import { CardsNotFoundError, GetCardNetworkError, IoTsValidationError } from './errors'
import { Card } from './01-types'
import { pipe } from 'fp-ts/lib/function'

export const getAndParseCards = (getCards: GetCards) => (
  userId: string
): TE.TaskEither<GetCardNetworkError | CardsNotFoundError | IoTsValidationError, Card[]> =>
  pipe(
    getCards(userId)
    // TODO: Implementation here
  )
