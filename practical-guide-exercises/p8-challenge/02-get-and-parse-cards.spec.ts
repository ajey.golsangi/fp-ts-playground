import { jestFn } from '../utils'
import { GetCards } from './card-api'
import * as TE from 'fp-ts/TaskEither'
import * as E from 'fp-ts/Either'
import { getAndParseCards } from './02-get-and-parse-cards'

describe('get-and-parse-cards', () => {
  const userId = 'user.id'

  it('should handle an error getting cards and return GetCardNetworkError', async () => {
    const getCards = jestFn<GetCards>().mockReturnValue(TE.left(new Error('Error!')))

    const res = await getAndParseCards(getCards)(userId)()

    expect(E.isLeft(res) && res.left.type === 'GET_CARD_NETWORK').toBe(true)
  })

  it('should handle an empty card response and return CardsNotFoundError', async () => {
    const cards = []
    const getCards = jestFn<GetCards>().mockReturnValue(TE.right([]))

    const res = await getAndParseCards(getCards)(userId)()

    expect(E.isLeft(res) && res.left.type === 'CARDS_NOT_FOUND').toBe(true)
  })

  it('should handle a malformed card response and return IoTsValidationError', async () => {
    const cards = [
      {
        id: '1',
      },
      {
        network: 'VISA' as const,
        pan: '1234567890',
      },
    ]
    const getCards = jestFn<GetCards>().mockReturnValue(TE.right(cards))

    const res = await getAndParseCards(getCards)(userId)()

    expect(E.isLeft(res) && res.left.type === 'IO_TS_VALIDATION').toBe(true)
  })

  it('should handle a successful get cards response and return Card[]', async () => {
    const cards = [
      {
        id: '1',
        status: 'ACTIVE' as const,
      },
      {
        id: '2',
        status: 'INACTIVE' as const,
        network: 'VISA' as const,
        last4: '1234',
        fulfillment: {
          recipient_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
          recipient_mailing_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      },
    ]
    const getCards = jestFn<GetCards>().mockReturnValue(TE.right(cards))

    const res = await getAndParseCards(getCards)(userId)()

    expect(res).toStrictEqual(
      E.right([
        {
          id: '1',
          status: 'ACTIVE' as const,
        },
        {
          id: '2',
          status: 'INACTIVE' as const,
          network: 'VISA' as const,
          last4: '1234',
          fulfillment: {
            recipient_address: {
              first_name: 'Malcolm',
              last_name: 'McCormick',
              address_1: '2407 J St',
              address_2: 'Suite 300',
              city: 'Sacramento',
              state: 'CA',
              zip: '95816',
              country: 'USA',
            },
            recipient_mailing_address: {
              first_name: 'Tyler',
              last_name: 'Okonma',
              address_1: '53 Beach St',
              address_2: '2nd Floor',
              city: 'New York',
              state: 'NY',
              zip: '10013',
              country: 'USA',
            },
          },
        },
      ])
    )
  })
})
