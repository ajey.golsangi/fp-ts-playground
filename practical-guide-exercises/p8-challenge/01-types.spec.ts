import { Card, DisplayCard } from './01-types'
import * as E from 'fp-ts/Either'

describe('types', () => {
  describe('Card', () => {
    it('should handle an invalid input', () => {
      const input = {
        never: 'going',
        to: 'give',
        you: 'up',
      }
      expect(Card.is(input)).toBe(false)
    })

    it('should handle a valid input with required fields', () => {
      const input = {
        id: '1',
        status: 'ACTIVE' as const,
      }
      expect(Card.is(input)).toBe(true)
      expect(Card.decode(input)).toEqual(
        E.right({
          id: '1',
          status: 'ACTIVE' as const,
        })
      )
    })

    it('should handle a valid input with required and optional fields - 1', () => {
      const input = {
        id: '1',
        status: 'ACTIVE' as const,
        network: 'VISA' as const,
        last4: '1234',
        fulfillment: {
          recipient_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
          recipient_mailing_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      }
      expect(Card.is(input)).toBe(true)
      expect(Card.decode(input)).toEqual(
        E.right({
          id: '1',
          status: 'ACTIVE' as const,
          network: 'VISA' as const,
          last4: '1234',
          fulfillment: {
            recipient_address: {
              first_name: 'Malcolm',
              last_name: 'McCormick',
              address_1: '2407 J St',
              address_2: 'Suite 300',
              city: 'Sacramento',
              state: 'CA',
              zip: '95816',
              country: 'USA',
            },
            recipient_mailing_address: {
              first_name: 'Tyler',
              last_name: 'Okonma',
              address_1: '53 Beach St',
              address_2: '2nd Floor',
              city: 'New York',
              state: 'NY',
              zip: '10013',
              country: 'USA',
            },
          },
        })
      )
    })

    it('should handle a valid input with required and optional fields - 2', () => {
      const input = {
        id: '1',
        status: 'ACTIVE' as const,
        network: 'VISA' as const,
        fulfillment: {
          recipient_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        },
      }
      expect(Card.is(input)).toBe(true)
      expect(Card.decode(input)).toEqual(
        E.right({
          id: '1',
          status: 'ACTIVE' as const,
          network: 'VISA' as const,
          fulfillment: {
            recipient_address: {
              first_name: 'Malcolm',
              last_name: 'McCormick',
              address_1: '2407 J St',
              address_2: 'Suite 300',
              city: 'Sacramento',
              state: 'CA',
              zip: '95816',
              country: 'USA',
            },
          },
        })
      )
    })
  })

  describe('DisplayCard', () => {
    it('should handle an invalid input', () => {
      const input = {
        never: 'going',
        to: 'let',
        you: 'down',
      }
      expect(DisplayCard.is(input)).toBe(false)
    })

    it('should handle a valid input with only required fields', () => {
      const input = {
        id: '1',
        status: 'ACTIVE' as const,
        network: 'VISA',
        display_name: 'Card x1234',
      }
      expect(DisplayCard.is(input)).toBe(true)
      expect(DisplayCard.decode(input)).toStrictEqual(
        E.right({
          id: '1',
          status: 'ACTIVE' as const,
          network: 'VISA',
          display_name: 'Card x1234',
        })
      )
    })

    it('should handle a valid input with required and optional fields', () => {
      const input = {
        id: '1',
        status: 'ACTIVE' as const,
        network: 'VISA',
        display_name: 'Card x1234',
        mail_to_address: {
          first_name: 'Malcolm',
          last_name: 'McCormick',
          address_1: '2407 J St',
          address_2: 'Suite 300',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
      }
      expect(DisplayCard.is(input)).toBe(true)
      expect(DisplayCard.decode(input)).toStrictEqual(
        E.right({
          id: '1',
          status: 'ACTIVE' as const,
          network: 'VISA',
          display_name: 'Card x1234',
          mail_to_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        })
      )
    })
  })
})
