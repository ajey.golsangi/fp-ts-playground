import { getCardsHandler } from './00-challenge'
import { BaseCard, GetCards } from './card-api'
import { jestFn } from '../utils'
import * as E from 'fp-ts/Either'
import * as TE from 'fp-ts/TaskEither'

describe('challenge', () => {
  const userId = 'user.id'
  it('should handle an error getting cards and return GetCardNetworkError', async () => {
    const getCards = jestFn<GetCards>().mockReturnValue(TE.left(new Error('Error!')))

    const res = await getCardsHandler({ userId, getCards })()

    expect(E.isLeft(res) && res.left.type === 'GET_CARD_NETWORK').toBe(true)
  })

  it('should handle no cards returned and return CardsNotFoundError', async () => {
    const cards: BaseCard[] = []
    const getCards = jestFn<GetCards>().mockReturnValue(TE.right(cards))

    const res = await getCardsHandler({ userId, getCards })()

    expect(E.isLeft(res) && res.left.type === 'CARDS_NOT_FOUND').toBe(true)
  })

  it('should handle a malformed cards response and return IoTsValidationError', async () => {
    const cards: BaseCard[] = [
      {
        id: '1',
      },
      {
        network: 'VISA',
        pan: '1234567890',
      },
    ]
    const getCards = jestFn<GetCards>().mockReturnValue(TE.right(cards))

    const res = await getCardsHandler({ userId, getCards })()

    expect(E.isLeft(res) && res.left.type === 'IO_TS_VALIDATION').toBe(true)
  })

  it('should handle no valid cards found and return NoValidCardsFoundError', async () => {
    const cards: BaseCard[] = [
      {
        id: '1',
        status: 'ACTIVE',
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'VISA',
      },
    ]
    const getCards = jestFn<GetCards>().mockReturnValue(TE.right(cards))

    const res = await getCardsHandler({
      userId,
      getCards,
      networkType: 'MASTERCARD',
      cardStatus: 'ACTIVE',
    })()

    expect(E.isLeft(res) && res.left.type === 'NO_VALID_CARDS_FOUND').toBe(true)
  })

  it('should handle successfully getting, validating, and format cards', async () => {
    const cards: BaseCard[] = [
      {
        id: '1',
        status: 'ACTIVE',
      },
      {
        id: '2',
        status: 'INACTIVE',
        network: 'VISA',
      },
      {
        id: '3',
        status: 'ACTIVE',
        network: 'MASTERCARD',
      },
      {
        id: '4',
        status: 'INACTIVE',
        network: 'MASTERCARD',
        last4: '1234',
      },
      {
        id: '5',
        status: 'ACTIVE',
        network: 'MASTERCARD',
        last4: '2345',
      },
      {
        id: '6',
        status: 'ACTIVE',
        network: 'MASTERCARD',
        last4: '3456',
        fulfillment: {
          recipient_mailing_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      },
      {
        id: '7',
        status: 'ACTIVE',
        network: 'MASTERCARD',
        last4: '4567',
        fulfillment: {
          recipient_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        },
      },
      {
        id: '8',
        status: 'ACTIVE',
        network: 'MASTERCARD',
        fulfillment: {
          recipient_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
          recipient_mailing_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      },
    ]
    const getCards = jestFn<GetCards>().mockReturnValue(TE.right(cards))

    const res = await getCardsHandler({
      userId,
      getCards,
      networkType: 'MASTERCARD',
      cardStatus: 'ACTIVE',
    })()

    expect(res).toEqual(
      E.right([
        {
          id: '3',
          status: 'ACTIVE',
          network: 'MASTERCARD',
          display_name: 'Card xXXXX',
        },
        {
          id: '5',
          status: 'ACTIVE',
          network: 'MASTERCARD',
          display_name: 'Card x2345',
        },
        {
          id: '6',
          status: 'ACTIVE',
          network: 'MASTERCARD',
          display_name: 'Card x3456',
          mail_to_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
        {
          id: '7',
          status: 'ACTIVE',
          network: 'MASTERCARD',
          display_name: 'Card x4567',
          mail_to_address: {
            first_name: 'Malcolm',
            last_name: 'McCormick',
            address_1: '2407 J St',
            address_2: 'Suite 300',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        },
        {
          id: '8',
          status: 'ACTIVE',
          network: 'MASTERCARD',
          display_name: 'Card xXXXX',
          mail_to_address: {
            first_name: 'Tyler',
            last_name: 'Okonma',
            address_1: '53 Beach St',
            address_2: '2nd Floor',
            city: 'New York',
            state: 'NY',
            zip: '10013',
            country: 'USA',
          },
        },
      ])
    )
  })
})
