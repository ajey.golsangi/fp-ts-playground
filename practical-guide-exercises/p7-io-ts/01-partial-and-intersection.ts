/**
 * Exercise:
 * - Implement the TransactionBase type as Transaction using io-ts
 */
import * as t from 'io-ts'

type TransactionBase = {
  transaction_id: string
  amount: number
  fee_amount?: number
  origin: {
    account_number: string
    routing_number: string
    alias?: string
  }
  receiver: {
    account_number: string
    routing_number: string
    alias?: string
  }
  metadata?: {
    with_fee: boolean
    fee_type?: string
    validated: boolean
    validation_details?: {
      validated_by: string
      validate_date?: string
    }
  }
}

// TODO Update below here
export const Transaction = {}

// No need to modify below here
export type Transaction = t.TypeOf<typeof Transaction>
