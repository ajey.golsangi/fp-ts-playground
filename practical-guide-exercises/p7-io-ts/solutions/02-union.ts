import * as t from 'io-ts'

type UserCreatedEventBase = {
  user_id: string
  event_name: 'USER_CREATED'
  create_date: string
}

type UserNameUpdatedEventBase = {
  user_id: string
  event_name: 'USER_NAME_UPDATED'
  first_name: string
  last_name?: string
}

type UserAddressUpdatedEventBase = {
  user_id: string
  event_name: 'USER_ADDRESS_UPDATED'
  address: {
    address_1: string
    address_2?: string
    city: string
    state: string
    zip: string
    country?: string
  }
  mailing_address?: {
    address_1: string
    address_2?: string
    city: string
    state: string
    zip: string
    country?: string
  }
}

type UserEventBase = UserCreatedEventBase | UserNameUpdatedEventBase | UserAddressUpdatedEventBase

const UserCreatedEvent = t.type({
  user_id: t.string,
  event_name: t.literal('USER_CREATED'),
  create_date: t.string,
})

const UserNameUpdatedEvent = t.intersection([
  t.type({
    user_id: t.string,
    event_name: t.literal('USER_NAME_UPDATED'),
    first_name: t.string,
  }),
  t.partial({
    last_name: t.string,
  }),
])

const UserAddress = t.intersection([
  t.type({
    address_1: t.string,
    city: t.string,
    state: t.string,
    zip: t.string,
  }),
  t.partial({
    address_2: t.string,
    country: t.string,
  }),
])
const UserAddressUpdatedEvent = t.intersection([
  t.type({
    user_id: t.string,
    event_name: t.literal('USER_ADDRESS_UPDATED'),
    address: UserAddress,
  }),
  t.partial({
    mailing_address: UserAddress,
  }),
])

export const UserEvent = t.union([UserCreatedEvent, UserNameUpdatedEvent, UserAddressUpdatedEvent])

export type UserEvent = t.TypeOf<typeof UserEvent>
