/**
 * Exercise:
 * - Implement the UserEventBase type as UserEvent using io-ts union
 */
import * as t from 'io-ts'

type UserCreatedEventBase = {
  user_id: string
  event_name: 'USER_CREATED'
  create_date: string
}

type UserNameUpdatedEventBase = {
  user_id: string
  event_name: 'USER_NAME_UPDATED'
  first_name: string
  last_name?: string
}

type UserAddressUpdatedEventBase = {
  user_id: string
  event_name: 'USER_ADDRESS_UPDATED'
  address: {
    address_1: string
    address_2?: string
    city: string
    state: string
    zip: string
    country?: string
  }
  mailing_address?: {
    address_1: string
    address_2?: string
    city: string
    state: string
    zip: string
    country?: string
  }
}

type UserEventBase = UserCreatedEventBase | UserNameUpdatedEventBase | UserAddressUpdatedEventBase

// TODO Update below here
export const UserEvent = {}

// No need to modify below here
export type UserEvent = t.TypeOf<typeof UserEvent>
