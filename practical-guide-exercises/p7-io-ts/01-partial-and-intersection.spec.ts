import { Transaction } from './01-partial-and-intersection'
import * as E from 'fp-ts/Either'

describe('partial-and-intersection', () => {
  it('handles invalid input', () => {
    const input = {
      not: 'a',
      valid: 'transaction',
    }
    expect(Transaction.is(input)).toBe(false)
  })

  it('handles input not containing required fields', () => {
    const input = {
      transaction_id: '1',
      amount: 100,
      origin: {
        account_number: '1',
        routing_number: '2',
      },
    }
    expect(Transaction.is(input)).toBe(false)
  })

  it('handles input only containing required fields', () => {
    const input = {
      transaction_id: '1',
      amount: 100,
      origin: {
        account_number: '1',
        routing_number: '2',
      },
      receiver: {
        account_number: '3',
        routing_number: '4',
      },
    }
    expect(Transaction.is(input)).toBe(true)
    expect(Transaction.decode(input)).toStrictEqual(
      E.right({
        transaction_id: '1',
        amount: 100,
        origin: {
          account_number: '1',
          routing_number: '2',
        },
        receiver: {
          account_number: '3',
          routing_number: '4',
        },
      })
    )
  })

  it('handles input with required and optional fields - 1', () => {
    const input = {
      transaction_id: '1',
      amount: 100,
      fee_amount: 10,
      origin: {
        account_number: '1',
        routing_number: '2',
        alias: 'first',
      },
      receiver: {
        account_number: '3',
        routing_number: '4',
        alias: 'second',
      },
      metadata: {
        with_fee: true,
        fee_type: 'REQUIRED',
        validated: true,
        validation_details: {
          validated_by: 'SYSTEM',
          validated_date: '2022-09-01',
        },
      },
    }
    expect(Transaction.is(input)).toBe(true)
    expect(Transaction.decode(input)).toStrictEqual(
      E.right({
        transaction_id: '1',
        amount: 100,
        fee_amount: 10,
        origin: {
          account_number: '1',
          routing_number: '2',
          alias: 'first',
        },
        receiver: {
          account_number: '3',
          routing_number: '4',
          alias: 'second',
        },
        metadata: {
          with_fee: true,
          fee_type: 'REQUIRED',
          validated: true,
          validation_details: {
            validated_by: 'SYSTEM',
            validated_date: '2022-09-01',
          },
        },
      })
    )
  })

  it('handles input with required and optional fields - 2', () => {
    const input = {
      transaction_id: '1',
      amount: 100,
      fee_amount: 10,
      origin: {
        account_number: '1',
        routing_number: '2',
      },
      receiver: {
        account_number: '3',
        routing_number: '4',
      },
      metadata: {
        with_fee: true,
        fee_type: 'REQUIRED',
        validated: false,
      },
    }
    expect(Transaction.is(input)).toBe(true)
    expect(Transaction.decode(input)).toStrictEqual(
      E.right({
        transaction_id: '1',
        amount: 100,
        fee_amount: 10,
        origin: {
          account_number: '1',
          routing_number: '2',
        },
        receiver: {
          account_number: '3',
          routing_number: '4',
        },
        metadata: {
          with_fee: true,
          fee_type: 'REQUIRED',
          validated: false,
        },
      })
    )
  })

  it('handles input with required and optional fields - 3', () => {
    const input = {
      transaction_id: '1',
      amount: 100,
      origin: {
        account_number: '1',
        routing_number: '2',
        aliast: 'first',
      },
      receiver: {
        account_number: '3',
        routing_number: '4',
      },
      metadata: {
        with_fee: false,
        fee_type: 'INVALID',
        validated: true,
        validation_details: {
          validated_by: 'SYSTEM',
        },
      },
    }
    expect(Transaction.is(input)).toBe(true)
    expect(Transaction.decode(input)).toStrictEqual(
      E.right({
        transaction_id: '1',
        amount: 100,
        origin: {
          account_number: '1',
          routing_number: '2',
          aliast: 'first',
        },
        receiver: {
          account_number: '3',
          routing_number: '4',
        },
        metadata: {
          with_fee: false,
          fee_type: 'INVALID',
          validated: true,
          validation_details: {
            validated_by: 'SYSTEM',
          },
        },
      })
    )
  })
})
