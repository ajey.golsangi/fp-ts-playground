/**
 * Exercise:
 * - Implement the UserBase type as User using io-ts strict
 * - Implement the AccountBase type Account using io-ts type
 */
import * as t from 'io-ts'

type UserBase = {
  user_id: string
  first_name: string
  last_name: string
  email: string
  age: number
  address: {
    address_1: string
    city: string
    state: string
    zip: string
    country: string
  }
}

type AccountBase = {
  account_id: string
  user_id: string
  account_numbers: number[]
  is_verified: boolean
}

// TODO Update below here
export const User = {}
export const Account = {}

// No need to modify below here
export type User = t.TypeOf<typeof User>
export type Account = t.TypeOf<typeof Account>
