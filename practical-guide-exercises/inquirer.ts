import { constVoid, flow, identity, pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/lib/TaskEither'
import * as inquirer from 'inquirer'

const getPrompt = flow(
  inquirer.prompt,
  TE.tryCatchK(identity, identity),
  TE.mapLeft(e => console.dir(e))
)

export const prompt = <T>(questions: inquirer.QuestionCollection<T>, initialAnswers?: Partial<T>) =>
  pipe(
    // Calling inquirer.prompt directly causes side effects (i.e. waiting for input)
    // We are deferring execution of the prompt here until the task is invoked.
    TE.rightIO(constVoid),
    TE.chain(() => getPrompt(questions, initialAnswers))
  )
