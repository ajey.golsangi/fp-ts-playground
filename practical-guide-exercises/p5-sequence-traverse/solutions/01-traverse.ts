/**
 * Be sure to read part 5 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-5
 *
 * Note: The above guide uses a deprecated method for traversing -- use T.traverseArray instead of A.array.traverse(T.task) or A.traverse(T.task)
 *
 * Exercise:
 *  - Modify add5Traverse to fix the type error by modifying the logic inside TE.traverseArray
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 01-traverse.ts
 *
 * Testing this file.
 *  - npm run test 01-traverse
 */

import { flow, pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/lib/TaskEither'
import * as E from 'fp-ts/lib/Either'
import { runIfCli } from '../../utils'

const add5 = (num?: number) => num === undefined ? E.left('invalid, number required') : E.right(num + 5)

export const add5Traverse = (numArr: number[]): TE.TaskEither<string, readonly number[]> =>
  pipe(
    numArr,
    TE.traverseArray(flow(add5, TE.fromEither))
    // TE.traverseArray(TE.fromEitherK(add5)) // - alternative
  )

// No need to modify below here, for running this file
const logAndReturnResult = (numArr: number[]) =>
  pipe(
    add5Traverse(numArr),
    TE.map(result =>
      pipe(
        console.dir(`add5Traverse([${numArr.join(',')}]):`),
        () => console.dir(result),
        () => result
      )
    )
  )

pipe(logAndReturnResult([1, 2, 3]), runIfCli(module))
