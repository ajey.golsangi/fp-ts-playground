# Welcome to Practical Guide to Fp-Ts Exercises

These exercises are meant to help reinforce the fp-ts concepts explained in the Practical Guide to fp-ts (https://rlee.dev/practical-guide-to-fp-ts-part-1)

There is a folder for each section that contains exercises. Feel free to attempt the exercises as you read through each section. There are also jest unit tests that you can run validate your changes. Many of the tests are in an error state and will work when you complete the corresponding exercise.

There is also a solutions folder for each section with the completed exercises (some contain a few alternate solutions too). Feel free to look at the solutions, but you will get more out of the exercises if you spend time attempting/completing them prior to looking at the solutions.

## Running tests

To run all tests, execute `npm run test` in the root directory of this project

To run a specific test, append the test name (i.e. `npm run test pipe-and-flow`)
