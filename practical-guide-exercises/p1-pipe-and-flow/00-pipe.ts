/**
 * Welcome!  These exercises follow the Practical Guide to fp-ts.
 * Be sure to read part 1 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-1
 *
 * Exercise:
 *  - Modify helloWithGreeting to use pipe
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 00-pipe.ts
 *
 * Testing this file.
 *  - npm run test pipe-and-flow
 */

import { pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/lib/TaskEither'
import { prompt, runIfCli } from '../utils'

const hello = (name: string) => `Hello ${name}!`

const appendSpace = (str: string) => str + ' '

const appendGreeting = (str: string) => `${str}Nice to meet you!`

export type GreetingFunction = (name: string) => string

// TODO modify this function to use pipe
export const helloWithGreeting: GreetingFunction = (name: string) => {
  const helloString = hello(name)
  const helloWithSpaceString = appendSpace(helloString)
  const helloWithGreetingString = appendGreeting(helloWithSpaceString)

  return helloWithGreetingString
}

// No need to modify below here, for running this file
pipe(
  prompt({ name: 'name', message: 'What is your name?' }),
  TE.map(answers => console.dir(helloWithGreeting(answers.name))),
  runIfCli(module)
)
