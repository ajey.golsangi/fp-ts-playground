import { helloWithGreeting } from './00-pipe'
import { goodDayMany } from './01-flow'

describe('pipe-and-flow', () => {
  it('helloWithGreeting returns the proper greeting', () => {
    const greeting = helloWithGreeting('Dan')
    expect(greeting).toBe('Hello Dan! Nice to meet you!')
  })
  it('goodDayMany returns the proper greeting', () => {
    const greeting = goodDayMany(['Dan', 'Andrew'])
    expect(greeting).toStrictEqual(['Hello Dan! Good day!', 'Hello Andrew! Good day!'])
  })
})
