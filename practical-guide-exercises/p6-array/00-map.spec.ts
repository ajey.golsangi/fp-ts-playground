import { add5AndConvertToString } from './00-map'

describe('map', () => {
  it.each`
    test                       | expectedOutput
    ${[]}                      | ${[]}
    ${[1]}                     | ${['6']}
    ${[0, -1, -2, -3, -4, -5]} | ${['5', '4', '3', '2', '1', '0']}
    ${[2, 4, 3, 4]}            | ${['7', '9', '8', '9']}
  `(`handles values $test`, ({ test, expectedOutput }) => {
    expect(add5AndConvertToString(test)).toStrictEqual(expectedOutput)
  })
})
