/**
 * Exercise
 * - Update getTotalWeightOfParcels to sum the weight of an array of Parcels using A.reduce
 */

import { pipe } from 'fp-ts/lib/function'
import * as A from 'fp-ts/Array'

interface Parcel {
  id: string
  weight: number
}

export const getTotalWeightOfParcels = (parcels: Parcel[]): number =>
  pipe(
    parcels
    // TODO Implementation here
  )
