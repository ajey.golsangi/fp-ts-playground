/**
 * Exercise
 * - Modify filterUsersByStatusAndAccountAge to filter an array of Users based off of an expected status
 *  and expected minimumAge (exclusive) using A.filter
 */
import { pipe } from 'fp-ts/function'
import * as A from 'fp-ts/Array'

type Status = 'ACTIVE' | 'INACTIVE'

export interface User {
  user_id: string
  status: Status
  account_age_months: number
}

export const filterUsersByStatusAndAccountAge = (
  users: User[],
  status: Status,
  minimumAge: number
): User[] =>
  pipe(
    users,
    A.filter(user => user.status === status),
    A.filter(user => user.account_age_months > minimumAge)
  )
