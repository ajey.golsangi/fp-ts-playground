/**
 * Exercise
 * - Update add5AndConvertToString to call add5ToNumber and convertNumberToString for every
 *   entry of an array using A.map
 */

import { pipe } from 'fp-ts/function'
import * as A from 'fp-ts/Array'

const add5ToNumber = (n: number): number => n + 5
const convertNumberToString = (n: number): string => `${n}`

export const add5AndConvertToString = (numbers: number[]): string[] =>
  pipe(
    numbers
    // TODO Implementation here
  )
