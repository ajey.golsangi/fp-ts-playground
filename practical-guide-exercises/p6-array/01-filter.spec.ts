import { filterUsersByStatusAndAccountAge, User } from './01-filter'

describe('filter', () => {
  const user1: User = {
    user_id: '1',
    status: 'ACTIVE',
    account_age_months: 1,
  }
  const user2: User = {
    user_id: '2',
    status: 'ACTIVE',
    account_age_months: 5,
  }
  const user3: User = {
    user_id: '3',
    status: 'ACTIVE',
    account_age_months: 10,
  }
  const user4: User = {
    user_id: '4',
    status: 'INACTIVE',
    account_age_months: 1,
  }
  const user5: User = {
    user_id: '5',
    status: 'INACTIVE',
    account_age_months: 5,
  }
  const user6: User = {
    user_id: '6',
    status: 'INACTIVE',
    account_age_months: 10,
  }

  const users = [user1, user2, user3, user4, user5, user6]

  it.each`
    testNum | status        | minAge | expectedOutput
    ${1}    | ${'ACTIVE'}   | ${0}   | ${[user1, user2, user3]}
    ${2}    | ${'ACTIVE'}   | ${5}   | ${[user3]}
    ${3}    | ${'ACTIVE'}   | ${20}  | ${[]}
    ${4}    | ${'INACTIVE'} | ${0}   | ${[user4, user5, user6]}
    ${5}    | ${'INACTIVE'} | ${5}   | ${[user6]}
    ${6}    | ${'INACTIVE'} | ${20}  | ${[]}
  `(`has expectedOutput for test case $testNum`, ({ status, minAge, expectedOutput }) => {
    expect(filterUsersByStatusAndAccountAge(users, status, minAge)).toStrictEqual(expectedOutput)
  })
})
