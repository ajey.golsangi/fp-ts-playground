import * as O from 'fp-ts/lib/Option'
import { add5IfDefined } from './00-option'

describe('option-map', () => {
  it('add5IfDefined returns an option with the proper calculation', () => {
    expect(add5IfDefined(10)).toStrictEqual(O.some(15))
  })
  it('add5IfDefined returns an option none if input is undefined', () => {
    expect(add5IfDefined()).toStrictEqual(O.none)
  })
})
