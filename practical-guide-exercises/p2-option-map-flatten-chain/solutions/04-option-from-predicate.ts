/**
 * Exercise
 * - Update getOptionNumberGreaterThan5 to return O.Option<number> containing input num if the value is greater than 5 (exclusive),
 *   otherwise return O.none
 */

import { pipe } from 'fp-ts/lib/function'
import * as O from 'fp-ts/Option'

export const getOptionNumberGreaterThan5 = (num: number): O.Option<number> =>
  pipe(
    num,
    O.fromPredicate(n => n > 5)
  )
