/**
 * Be sure to read part 2 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-2
 *
 * Exercise:
 *  - Modify add5IfDefined to return Option<number> instead of (number | undefined)
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 00-option.ts
 *
 * Testing this file.
 *  - npm run test 00-option
 */

import { pipe } from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../../utils'

const add5Option = O.map<number, number>(num => num + 5)

export const add5IfDefined = (number?: number): O.Option<number> =>
  pipe(
    // TODO modify this line to convert number to Option<number> and fix the type error
    O.fromNullable(number),
    add5Option
  )

// No need to modify below here, for running this file
const logAdd5 = (number?: number) =>
  pipe(console.dir(`add5IfDefined(${number}):`), () => console.dir(add5IfDefined(number)))

pipe(
  T.fromIO(() => pipe(logAdd5(10), () => logAdd5())),
  runIfCli(module)
)
