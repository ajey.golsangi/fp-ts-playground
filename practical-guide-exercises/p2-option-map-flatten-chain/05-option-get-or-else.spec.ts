import { getNumberLessThan10OrDefault, DefaultNumber } from './05-option-get-or-else'

describe('option-get-or-else', () => {
  it('handles numbers greater than 10', () => {
    expect(getNumberLessThan10OrDefault(20)).toStrictEqual(DefaultNumber)
  })

  it('handles numbers equal to 10', () => {
    expect(getNumberLessThan10OrDefault(10)).toStrictEqual(DefaultNumber)
  })

  it('handles numbers less than 10', () => {
    expect(getNumberLessThan10OrDefault(5)).toStrictEqual(5)
  })
})
