/**
 * Exercise
 * - Update getNumberLessThan10OrDefault to return the input num if less than 10 or
 *   DefaultNumber if greater than or equal to 10 using O.getOrElse
 */

import { pipe } from 'fp-ts/function'
import * as O from 'fp-ts/Option'

export const DefaultNumber = Number.MAX_SAFE_INTEGER

export const getNumberLessThan10OrDefault = (num: number): number =>
  pipe(
    num
    // TODO Implementation here
  )
